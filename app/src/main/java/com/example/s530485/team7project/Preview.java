package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Preview extends AppCompatActivity {
 String e1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        TextView textView3 = (TextView)findViewById( R.id.textView3 );
        Intent intent = getIntent();
        e1 = intent.getStringExtra( "e1" );
        textView3.setText(e1);
        Toast.makeText( getApplicationContext(),e1, Toast.LENGTH_LONG ).show();
    }
    public void userAccount(View v) {
        Toast.makeText(getApplicationContext(),"Your Booking is Confirmed", Toast.LENGTH_SHORT).show();
      Intent intent = new Intent(this, BookingInfo.class);
       startActivity(intent);
    }
}

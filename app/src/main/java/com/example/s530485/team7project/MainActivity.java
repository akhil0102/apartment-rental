package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Parse.initialize(this);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseUser.logOut();
    }

    public void Onview (View view){
        Intent ini = new Intent(this, signup.class );
        startActivity( ini );
    }

    public void userLogon(View view){
        Intent i1 = new Intent(this,login.class);
        startActivity(i1);
    }

    public void adminLogon(View view){
        Intent i1 = new Intent(this,AdminLogin.class);
        startActivity(i1);
    }
}

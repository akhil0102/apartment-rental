package com.example.s530485.team7project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class RoomInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_info);
        EditText e1 = (EditText) findViewById(R.id.editText4);
        EditText e2 = (EditText) findViewById(R.id.editText5);
        EditText e3 = (EditText) findViewById(R.id.editText6);
        EditText e4 = (EditText) findViewById(R.id.editText7);
        EditText e5 = (EditText) findViewById(R.id.editText8);
        EditText roomType = (EditText)findViewById(R.id.roomType);

        e1.setText("12345678");
        e2.setText("Pavan");
        e3.setText("Charles");
        e4.setText("$600");
        e5.setText("6 months");
        roomType.setText("double");

    }
}

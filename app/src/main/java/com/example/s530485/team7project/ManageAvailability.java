package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class ManageAvailability extends AppCompatActivity {
    EditText e1;
    EditText e2;
    EditText e3;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_availability);
        e1 = (EditText) findViewById(R.id.editText3);
        e2 = (EditText) findViewById(R.id.editText);
        e3 = (EditText) findViewById(R.id.editText2);
        btn= (Button) findViewById(R.id.button9);
        btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ParseObject availability = new ParseObject("Availability");
            availability.put("available", e1.getText().toString());
            availability.put("filled", e2.getText().toString());
            availability.put("vacant",e3.getText().toString());
            availability.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    System.out.println("Saved Callback");
                }
            });
            Intent i = new Intent(ManageAvailability.this,AdminAccount.class);
            startActivity(i);
            Toast.makeText(ManageAvailability.this, "Update successful", Toast.LENGTH_LONG)
                    .show();

        }
    });
    }

}

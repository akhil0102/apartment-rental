# Project Name: Apartment Rentals
# Team Name: AnneDroid's
# Team Members:

- Lakshman Pavan Kumar Vudutha

- Akhil Ravipati

- Venkata Siva Sai Nadipeneni

- Tapan Kumar Jella

#Links

- Clickable link for repository of Project: https://bitbucket.org/S530485/apartment-rental 

#Introduction:
  We as a team developed an android app called "Apartment Rentals". This app is used to book a room in the apartment for rent. The objective of this mobile application is to provide apartment rental service to users. The users have to register, sign-in, check the availability and book the apartment. The admin manages the room allotment.

#Technologies & Tools:

- Frontend(User Interface): Android Studio 3.0.1

- Backend(Database): Back4App (Parse Sevrer Platform)

- Testing: Genymotion 2.11.0 (Google Nexus 5 - 4.4.4 - API 19 - 1080*1920 )

- minSdkVersion 19

- targetSdkVersion 26

#Description of the activities: 
 Our app contains different activities. They are

  - Home Page: This page contains Userlogin, Admin Login and Signup Buttons. Admin uses the Admin login. And user use userlogin and Signup.

  - Registration Page: This is used by the new users to register into app.

  - User SignIn Page: This page is used by the registered users to get into the app.

  - My account Page: When the user entered the correct credentials this page is opened. This contains Signout, Booking Info, Book a room and Manage booking buttons.

  - Room availability Page: When the user clicks on the Book a room button in My account Page this page is opened. This page contains the availability status like number of rooms available, filled and vacant. Also contains a book button to move forward for booking.

  - Variations: When the user clicks on the Book button in Room availabiltiy Page this page is opened. This page contains the buttons Single(Small), Single(Medium), Single(Large), Double Bed Room. 

  - Preview Page: When the user clicks on the particluar button in Vriations page, respective page will be opened displaying the price of rent and confirm booking button to confirm his booking.

  - Manage Booking page: When the user clicks on the Manage booking button in My account Page this page is opened. This page conatins the cancel and change room buttons. 

  - Feedback Page: When the user clicks on the particluar button in Manage booking page, respective page will be opened. This is used to take the feedback from the user about the rooms. 

  - Booking Information: When the user clicks on the Booking Information button in My account Page this page is opened.This page conatins the complete booking informatiom about the user

  - Admin Account: This page can only be opened by admin. When the admin entered the correct credentials this page is opened. This page contains Manage bookings, which is used to manage the bookings made by user, Confirm Booking, which is used to confirm the bookings made by the user, and Update availability, which is used to update the availability status in Room availability page.

#List of features and Completion Status: 
   - User Login and Register - Completed
   
   - Cloud Database Connectivity -Completed
   
   - Admin Login - Completed
   
   - View and Manage Availability - Completed
   
   - View various rooms and book - Completed
   
   - Feedback Page - Completed	
   
   - Booking Information, Manage booking, Update Availability - In progress

#Testing credentials examples:
 - Admin credentials: USERNAME: admin@gmail.com, PASSWORD: 1234
 
 - User credentials: USERNAME: pavan, PASSWORD: pavan

#Contact Team Details: 

  - Lakshman Pavan Kumar Vudutha - S530664@nwmissouri.edu
  
  - Akhil Ravipati - S530485@nwmissouri.edu
  
  - Tapan Jella - S530713@nwmissouri.edu
  
  - Venkata Siva Sai Nadipeneni - S530665@nwmissouri.edu

package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLogin extends AppCompatActivity {
    private EditText userEmail;
    private EditText userPassword;
    private Button userSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_admin_login );
        userEmail = (EditText) findViewById( R.id.userEmail );
        userPassword = (EditText) findViewById( R.id.userPassword );
        userSignin = (Button) findViewById( R.id.userSignin );

        userSignin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate( userEmail.getText().toString(),userPassword.getText().toString() );
            }
        } );
    }

    public void validate(String Email, String password) {
        if ((Email.equals( "admin@gmail.com" )) && (password.equals("1234")))

        {
            Intent i = new Intent( this, AdminAccount.class );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity( i );
            Toast.makeText(getApplicationContext(),"Login Success", Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(getApplicationContext(),"Check your email and password", Toast.LENGTH_SHORT).show();

        }
    }
}
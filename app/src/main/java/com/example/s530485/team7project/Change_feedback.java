package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Change_feedback extends AppCompatActivity {
    Button change;
    EditText ed1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_change_feedback );
        change = (Button) findViewById(R.id.submit);
        ed1 = (EditText) findViewById(R.id.editText);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"pavanvudutha@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Reason for Changing:");
                i.putExtra(Intent.EXTRA_TEXT   ,ed1.getText().toString());
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(Change_feedback.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
public  void OnView (View v){
    Toast.makeText(getApplicationContext(),"Your Feedback is Submitted ", Toast.LENGTH_SHORT).show();
        Intent i = new Intent( this,RoomAvailability.class );
        startActivity( i );
}
}

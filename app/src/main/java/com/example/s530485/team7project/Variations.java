package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Variations extends AppCompatActivity {
  String a = "$400";
  String b = "$480";
  String c = "$550";
  String d = "$600";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_variations);
        Button button8 = (Button)findViewById( R.id.button8 );
        Button button9 = (Button)findViewById( R.id.button9 );
        Button button10 = (Button)findViewById( R.id.button10 );
        Button button11 = (Button)findViewById( R.id.button11);
        button8.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Variations.this,Preview.class);
                intent.putExtra("e1", a );
                startActivity(intent);
            }
        } );
        button9.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( Variations.this , Preview.class );
                intent.putExtra("e1", b );
                startActivity(intent);
            }
        } );
        button10.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( Variations.this, Preview.class );
                intent.putExtra( "e1", c );
                startActivity( intent );
            }
        });
        button11.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( Variations.this, Preview.class );
                intent.putExtra( "e1", d );
                startActivity( intent );
            }
        });
    }
}


package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class cancel_feedback extends AppCompatActivity {
    Button cancel;
    EditText ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cancel_feedback );
        cancel = (Button) findViewById(R.id.sub);
        ed = (EditText) findViewById(R.id.editText4);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"pavanvudutha@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Reason for Cancellation:");
                i.putExtra(Intent.EXTRA_TEXT   ,ed.getText().toString());
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(cancel_feedback.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void OnView(View v){
        Toast.makeText(getApplicationContext(),"Your Feedback is  Submitted", Toast.LENGTH_SHORT).show();
        Intent in = new Intent( this,BookingInfo.class );
        startActivity( in );
    }
}

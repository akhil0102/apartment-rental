package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AdminAccount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_account);
    }

    public void logOut(View view){
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        Toast.makeText(AdminAccount.this, "Logout Successful", Toast.LENGTH_LONG)
                .show();
    }

    public void confirmBookings(View view){
        Intent i = new Intent(this, ConfirmBookings.class);
        startActivity(i);
    }

    public void manageBookings(View view){
        Intent i = new Intent(this, MangeBookingsAdmin.class);
        startActivity(i);
    }

    public void updateAvailability(View view){
        Intent i = new Intent(this, ManageAvailability.class);
        startActivity(i);
    }
}

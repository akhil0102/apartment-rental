package com.example.s530485.team7project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.parse.ParseUser;

public class BookingInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_info);
    }
    public void onClick(View v){
        Intent in = new Intent(this,RoomAvailability.class);
        startActivity(in);
    }
    public void onClick2(View v){
        Intent in2 = new Intent(this,Managebooking.class);
        startActivity(in2);
    }
    public void onClick3(View v){
        Intent in3 = new Intent(this,RoomInfo.class);
        startActivity(in3);
    }
    public void onClick4(View v){
        Intent in4 = new Intent(this,MainActivity.class);
        ParseUser.logOut();
        in4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in4);
        Toast.makeText(this, "Sign out successful",Toast.LENGTH_SHORT).show();
    }
}
